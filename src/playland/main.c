#include <config.h>
#include <i18n.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glitz.h>
#include <xcb/xcb.h>
#include <cairo/cairo.h>
#include <cairo/cairo-glitz.h>

int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
	printf(_("Hello, World!\n"));

	return 0;
}
